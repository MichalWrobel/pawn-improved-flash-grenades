Another CS 1.6 plugin that allows you to change color of grenade and it creates light when flash grenade explodes.
Example usage: make flashes more eye friendly by changing its color.

Cvars:
flash_random [0/1] Make flash color random for each explosion, flash_add [0/1] Turn on/off additional light effect, flash_addRange [0/1], Range of additional light effect, flash_red [0/255], flash_green [0/255], flash_blue [0/255] To set up RGB color of flash

![ScriptInAction#1](https://bytebucket.org/MichalWrobel/pawn-improved-flash-grenades/raw/b3d7b6d21d18297f23028fa8c093aa4e5d9a3d28/Flashes1.jpg)
![ScriptInAction#2](https://bytebucket.org/MichalWrobel/pawn-improved-flash-grenades/raw/b3d7b6d21d18297f23028fa8c093aa4e5d9a3d28/Flashes2.jpg)