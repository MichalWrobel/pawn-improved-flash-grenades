/* 
	Name: 		Improved flash grenades
	Author: 	Michał Wróbel
	Year: 		2015
	License: 	MIT
*/

#include <amxmodx>
#include <engine>
#include <fakemeta>
 
new userFlashed;
new redColor;
new greenColor;
new blueColor;
new effectRadius;

public plugin_init() {
	
	register_plugin("Improved flash grenades","","");
	/*
	b - single client event flag
	e - only for alive users flag
	1,2,3,4 - any numbers for additional conditions
	255, 255, 255 - RGB white color( flashes are white by default )
	>199 - more than 199 alpha
	*/
	register_event("ScreenFade","eventFlashed","be","4=255","5=255","6=255","7>199");
	
	//Setting up a variable to handle the flash
	userFlashed = get_user_msgid("ScreenFade");
	
	//If we want flashes colours to be random each time 
	register_cvar("flash_random", "0");
	
	//Some cvars to customize our static flashes
	redColor = register_cvar("flash_red", "0");
	greenColor = register_cvar("flash_green", "255");
	blueColor = register_cvar("flash_blue", "255");
	
	//Additional effect
	register_cvar("flash_add", "1");
	
	//range of effect
	register_cvar("flash_addRange", "65");
	effectRadius = get_cvar_num("flash_addRange");
	
	// Setting up a special fakemeta forward function to handle additional effect
	register_forward(FM_EmitSound,"emitsoundForward");

}
public eventFlashed(id){
	
	// Checking if user want random flash colors
	if(get_cvar_num("flash_random"))
	{
		// random_num func get two properties that setting range, 255 is a maximum range for rgb colors
		redColor = random_num(0, 255);
		greenColor = random_num(0, 255);
		blueColor = random_num(0, 255);
	}
	// Setting up a flashed user effect, more information here: http://www.amxmodx.org/api/message_const
	message_begin( MSG_ONE,userFlashed,{0,0,0},id );
	write_short(read_data(1));	// Duration
	write_short(read_data(2));	// Hold time
	write_short(read_data(3));	// Fade type
	write_byte (redColor);		// Red
	write_byte (greenColor);	// Green
	write_byte (blueColor)	;	// Blue
	write_byte (read_data(7));	// Alpha
	message_end();
	 
	return PLUGIN_HANDLED;
}

public emitsoundForward(ent,channel,const sample[],Float:volume,Float:attenuation,fFlags,pitch)
{
	// Checking if its a flashbang, this is how we found a flashbang
	if(!equali(sample,"weapons/flashbang-1.wav") && !equali(sample,"weapons/flashbang-2.wav"))
	
	return FMRES_IGNORED; // Calls target function, returns normal value
	// Feature disabled
	if(!get_cvar_num("flash_add")) 
		return FMRES_IGNORED;
	// Our additional effect function
	addEffect(ent);
	return FMRES_IGNORED; 
	
	// More information about fakemeta_const here: http://www.amxmodx.org/api/fakemeta_const
}
public addEffect(flash){
	//Checking our entity correctness
	if(!is_valid_ent(flash)) 
		return; //stop if not valid
		
	//Setting up a variable to handle the origin of flash grenade
	new Float:origin[3]; 	// [3] means 3 dimensions X,Y,Z. 
				//Please notice that origin array elements are counted from 0 so new array[3] elements are array[0], array[1], array[2]
	
	//Getting a origin of a flash
	entity_get_vector(flash, EV_VEC_origin, origin);
	
	//Check if flash_random cvar is enabled
	if(get_cvar_num("flash_random"))
	{
		//random_num func get two properties that setting range, 255 is a maximum range for rgb colors
		redColor = random_num(0, 255);
		greenColor = random_num(0, 255);
		blueColor = random_num(0, 255);
	}
	// Setting up a flash effect, more information here: http://www.amxmodx.org/api/message_const
	message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
	write_byte(27); 				// There are many different effects, we're using TE_DLIGHT here
	write_coord(floatround(origin[0])); 		// X rounded because float in write_coord causes an error
	write_coord(floatround(origin[1]));		// Y same here
	write_coord(floatround(origin[2]));		// Z same here
	write_byte(effectRadius); 			// Radius
	write_byte(redColor);				// Red
	write_byte(greenColor); 			// Green
	write_byte(blueColor); 				// Blue
	write_byte(15); 				// Life
	write_byte(70); 				// Decay rate
	message_end();

}
